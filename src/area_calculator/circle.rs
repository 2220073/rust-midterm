use std::io::{self, Write};

/// Takes user input for the area of a circle and computes its radius.
///
/// This function prompts the user to input the area of a circle, validates the input
/// to ensure it is a positive number, computes the radius, and displays the result.
///
/// # Note
///
/// - This function assumes that the input for the area is a positive number.
/// - The program pauses and waits for the user to press enter before continuing.
pub fn input_circle() {
    let mut radius: f64;
    let mut input = String::new();
    let kbd = io::stdin();

    // Input loop for the area of the circle
    loop {
        print!("Enter the area of the circle: ");
        io::stdout().flush().unwrap();
        input.clear();
        kbd.read_line(&mut input).unwrap();
        radius = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive length for the area");
                continue;
            }
        };
        break;
    }

    // Calculate and display the radius of the circle
    println!("The radius of the circle is: {}", radius);

    // Calculate and display the area of the circle
    let area = compute_area(radius);
    println!("The area of the circle is: {}", area);

    println!();
    println!("Press enter to continue...");
    io::stdin().read_line(&mut String::new()).unwrap();
}

/// Computes the area of a circle based on the provided radius.
///
/// # Arguments
///
/// * `radius` - The radius of the circle.
///
/// # Returns
///
/// Returns `area_of_circle` the calculated area of the circle.
///
/// # Note
///
/// This function uses the mathematical constant π (pi) from the standard library.
pub fn compute_area(radius: f64) -> f64 {
    std::f64::consts::PI * radius * radius
}
