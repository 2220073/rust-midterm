use std::io::{self, Write};

/// Represents a triangle with a given base and height.
pub struct Triangle {
    base: f64,
    height: f64,
}

impl Triangle {
    /// Creates a new `Triangle` instance with the given base and height.
    pub fn new(base: f64, height: f64) -> Self {
        Self { base, height }
    }

    /// Calculates and returns the area of the triangle.
    ///
    /// # Returns
    ///
    /// Returns the calculated area of the triangle.
    pub fn area(&self) -> f64 {
        (self.base * self.height) / 2.0
    }

    /// Sets the base of the triangle.
    ///
    /// # Arguments
    ///
    /// * `base` - The new value for the base of the triangle.
    pub fn set_base(&mut self, base: f64) {
        self.base = base;
    }

    /// Sets the height of the triangle.
    ///
    /// # Arguments
    ///
    /// * `height` - The new value for the height of the triangle.
    pub fn set_height(&mut self, height: f64) {
        self.height = height;
    }
}

/// Takes user input for the base and height of a triangle and computes its area.
///
/// This function prompts the user to input the base and height of a triangle,
/// validates the input to ensure they are positive numbers, creates a `Triangle`
/// instance, computes the area of the triangle, and displays the result.
///
/// # Note
///
/// - This function assumes that the inputs for the base and height are positive numbers.
/// - The program pauses and waits for the user to press enter before continuing.
///
pub fn input_triangle() {
    let base_triangle: f64;
    let height_triangle: f64;

    let kbd = io::stdin();

    // Input loop for the base of the triangle
    loop {
        print!("Enter the base of the triangle: ");
        io::stdout().flush().unwrap();

        let mut input = String::new();
        kbd.read_line(&mut input).unwrap();

        base_triangle = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive base");
                continue;
            }
        };
        break;
    }

    // Input loop for the height of the triangle
    loop {
        print!("Enter the height of the triangle: ");
        let mut input = String::new();
        io::stdout().flush().unwrap();
        input.clear();
        kbd.read_line(&mut input).unwrap();

        height_triangle = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive height");
                continue;
            }
        };
        break;
    }

    // Create a Triangle instance and calculate/display the area
    let triangle = Triangle::new(base_triangle, height_triangle);
    println!("The area of the triangle is: {}", triangle.area());
    println!();
    println!("Press enter to continue...");
    io::stdin().read_line(&mut String::new()).unwrap();
}
