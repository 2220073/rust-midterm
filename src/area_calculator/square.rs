use std::io::{self, Write};
/// Takes user input for the length of the side of a square, computes its area, and displays the result.
///
/// This function prompts the user to input the length of the side of a square, validates the input to
/// ensure it is a positive number, computes the area of the square, and displays the result.
///
/// # Note
///
/// - This function assumes that the input for the length is a positive number.
/// - The program pauses and waits for the user to press enter before continuing.
///
pub fn input_square() {
    let mut length: f64;
    let mut input = String::new();
    let kbd = io::stdin();

    println!("This part of the program will compute the area of a square.");

    // Input loop for the length of the side of the square
    loop {
        println!("Enter the length of the side of a square:");
        io::stdout().flush().unwrap();
        input.clear();
        kbd.read_line(&mut input).unwrap();

        length = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive length");
                continue;
            }
        };
        break;
    }

    // Calculate and display the area of the square
    println!("The area of the square is: {}", compute_area(length));

    println!();
    println!("Press enter to continue...");
    io::stdin().read_line(&mut String::new()).unwrap();
}

/// Calculates the area of a square given the length of its side.
///
/// # Arguments
///
/// * `length` - The length of the side of the square.
///
/// # Returns
///
/// Returns the calculated area of the square.
///
pub fn compute_area(length: f64) -> f64 {
    length * length
}

