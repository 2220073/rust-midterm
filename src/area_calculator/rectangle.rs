
use std::io::{self, Write};
/// This function prompts the user to input the length and width of a rectangle,
/// validates the input to ensure they are positive numbers, computes the area of
/// the rectangle, and displays the result.
///
/// # Note
///
/// - This function assumes that the inputs for the length and width are positive numbers.
/// - The program pauses and waits for the user to press enter before continuing.
pub fn input_rectangle() {
    let mut length_rectangle: f64;
    let mut width_rectangle: f64;
    let mut input = String::new();
    let kbd = io::stdin();

    // Input loop for the length of the rectangle
    loop {
        print!("Enter the length of the rectangle: ");
        io::stdout().flush().unwrap();
        input.clear();
        kbd.read_line(&mut input).unwrap();
        length_rectangle = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive length");
                continue;
            }
        };
        break;
    }

    // Input loop for the width of the rectangle
    loop {
        print!("Enter the width of the rectangle: ");
        io::stdout().flush().unwrap();
        input.clear();
        kbd.read_line(&mut input).unwrap();
        width_rectangle = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive width");
                continue;
            }
        };
        break;
    }

    // Calculate and display the area of the rectangle
    print!("The area of the rectangle is: {}", compute_area_rectangle(length_rectangle, width_rectangle));
    println!();
    println!("Press enter to continue...");
    io::stdin().read_line(&mut String::new()).unwrap();
}

/// Computes the area of a rectangle based on the provided dimensions.
///
/// # Arguments
///
/// * `length` - The length of the rectangle.
/// * `width` - The width of the rectangle.
///
/// # Returns
///
/// Returns the calculated area of the rectangle.
///
pub fn compute_area_rectangle(length_rectangle: f64, width_rectangle: f64) -> f64 {
    length_rectangle * width_rectangle
}
