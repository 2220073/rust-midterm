use std::io::{self, Write};

/// This function takes user input for the dimensions of a trapezoid and computes its area.
///
/// This function prompts the user to input the lengths of the two bases and the height of a trapezoid,
/// validates the input to ensure they are positive numbers, computes the area of the trapezoid,
/// and displays the result.
///
/// # Note
///
/// - This function assumes that the inputs for the bases and height are positive numbers.
/// - The program pauses and waits for the user to press enter before continuing.
///
pub fn input_trapezoid() {
    let mut base1_trapezoid: f64;
    let mut base2_trapezoid: f64;
    let mut height_trapezoid: f64;
    let mut input = String::new();
    let kbd = io::stdin();

    // Input loop for the length of the first base of the trapezoid
    loop {
        print!("Enter the length of the 1st base of the Trapezoid: ");
        io::stdout().flush().unwrap();
        input.clear();
        kbd.read_line(&mut input).unwrap();
        base1_trapezoid = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive length for the base 1");
                continue;
            }
        };
        break;
    }

    // Input loop for the length of the second base of the trapezoid
    loop {
        print!("Enter the length of the 2nd base of the Trapezoid: ");
        io::stdout().flush().unwrap();
        input.clear();
        kbd.read_line(&mut input).unwrap();
        base2_trapezoid = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive length for the base 2");
                continue;
            }
        };
        break;
    }

    // Input loop for the height of the trapezoid
    loop {
        print!("Enter the height of the trapezoid: ");
        io::stdout().flush().unwrap();
        input.clear();
        kbd.read_line(&mut input).unwrap();
        height_trapezoid = match input.trim().parse() {
            Ok(num) if num > 0.0 => num,
            _ => {
                println!("Please enter a positive height");
                continue;
            }
        };
        break;
    }

    // Calculate and display the area of the trapezoid
    println!(
        "\nThe area of the trapezoid is {}",
        compute_area(base1_trapezoid, base2_trapezoid, height_trapezoid)
    );
    println!();
    println!("Press enter to continue...");
    input.clear();
    kbd.read_line(&mut input).unwrap();
}

/// This function computes the area of a trapezoid based on the provided dimensions.
///
/// # Arguments
///
/// * `base1_trapezoid` - The length of the first base of the trapezoid.
/// * `base2_trapezoid` - The length of the second base of the trapezoid.
/// * `height_trapezoid` - The height of the trapezoid.
///
/// # Returns
///
/// Returns the calculated area of the trapezoid.
///
pub fn compute_area(base1_trapezoid: f64, base2_trapezoid: f64, height_trapezoid: f64) -> f64 {
    ((base1_trapezoid + base2_trapezoid) * height_trapezoid) / 2.0
}
