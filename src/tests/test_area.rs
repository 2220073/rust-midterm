use crate::area_calculator::{circle,rectangle,parallelogram,trapezoid, triangle};
use crate::area_calculator::triangle::TraitName;

#[test]
fn test_compute_area_circle(){
    assert_eq!(circle::compute_area(2.0),12.566370614359172);
    assert_eq!(circle::compute_area(3.0),28.274333882308138);
}

#[test]
fn test_compute_area_rectangle(){
    assert_eq!(rectangle::compute_area_rectangle(8.0, 9.0),72.0);
    assert_eq!(rectangle::compute_area_rectangle(23.4, 12.0),280.79999999999995);
}

#[test]
fn test_compute_area_parallelogram(){
    assert_eq!(parallelogram::compute_area(6.0, 7.0), 42.0);
    assert_eq!(parallelogram::compute_area(23.1, 7.0), 161.70000000000002);
}

#[test]
fn test_compute_area_trapezoid() {
    assert_eq!(trapezoid::compute_area(2.0, 3.0, 4.0), 10.0);
    assert_eq!(trapezoid::compute_area(3.4, 12.2, 3.0), 23.4);
}

#[test]
fn test_compute_area_triangle() {
    let mut t = triangle::Triangle::new(5.0, 6.0);
    assert_eq!(t.area(),15.0);

    t.set_base(7.7);
    t.set_height(8.8);
    assert_eq!(t.area(),33.88);
}