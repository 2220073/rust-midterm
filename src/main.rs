//! # About
//!
//! This program calculates the area of different shapes.
//! 
//! ## Usage
//!
//! Import the necessary modules and invoke the main function.
//!
//! ```rust
//! use std::io::{self, Write};
//! mod area_calculator;
//!
//! fn main() {
//!     // ... (rest of your main function)
//! }
//! ```
use std::io::{self, Write}; 
mod area_calculator;

#[cfg(test)]
#[doc(hidden)]
mod tests;

#[doc(hidden)]
fn main() {
    show_introduction();

    loop {
        show_main_menu();
        let my_choice = choice_selector(8);

        match my_choice {
            1 => area_calculator::circle::input_circle(),
            2 => area_calculator::square::input_square(),
            3 => area_calculator::triangle::input_triangle(),
            4 => area_calculator::rectangle::input_rectangle(),
            5 => area_calculator::trapezoid::input_trapezoid(),
            6 => area_calculator::parallelogram::input_parallelogram(),
            7 => {
                println!("Thank you for using my program.");
                println!("Enjoy the rest of your day.");
                break;
            }
            _ => println!("Invalid choice. Please try again."),
        }
    }
}

#[doc(hidden)]
fn choice_selector(max: i32) -> i32 {
    loop {
        print!("Input the number corresponding to your choice: ");

        io::stdout().flush().unwrap();

        let mut input = String::new();

        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");

        match input.trim().parse::<i32>() {
            Ok(value) => {
                if value < 1 || value > max {
                    println!("Invalid Input! Please try again.");
                } else {
                    return value;
                }
            }
            Err(_) => {
                println!("Enter a number!");
            }
        }
    }
}

#[doc(hidden)]
fn show_introduction() {
    println!("\n\n\n");
    println!("College of Information and Computing Science");
    println!("          Saint Louis University            ");
    println!("               Baguio City                  ");
    println!("--------------------------------------------");
    println!("              All about Area               ");
    println!("\n");
    println!("                Programmers                ");
    println!("           Maxwell John Arzadon            ");
    println!("       Carliah Beatriz Del Rosario         ");
    println!("             Greta Belle Galo              ");
    println!("            Carl Joshua Lalwet             ");
    println!("            Jessalyn Mae Masaba            ");
    println!("           Jan Ivan Ezra Paguyo            ");

}

#[doc(hidden)]
fn show_main_menu() {
    println!("\n");
    println!("Math Routine Submenu");
    println!("--------------------------------------------");
    println!("1. Determine the area of a circle");
    println!("2. Determine the area of a square");
    println!("3. Determine the area of a triangle");
    println!("4. Determine the area of a rectangle");
    println!("5. Determine the area of a trapezoid");
    println!("6. Determine the area of a parallelogram");
    println!("7. Exit the Program");
    println!("--------------------------------------------");
}

